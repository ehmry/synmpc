# synmpc

Enques songs to MPD from Syndicate assertions. Uses the same `<xdg-open @uris [string ...]>` assertions as [xdg_open_ng](https://git.syndicate-lang.org/ehmry/xdg_open_ng).
