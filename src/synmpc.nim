# SPDX-FileCopyrightText: ☭ 2022 Emery Hemingway
# SPDX-License-Identifier: Unlicense

import std/asyncdispatch
import preserves
import syndicate
import mpdclient

type
  XdgOpen {.preservesRecord: "xdg-open".} = object
    uris: seq[string]
  ListenOn {.preservesRecord: "listen-on".} = ref object
    dataspace: Preserve[Ref]

bootDataspace("main") do (root: Ref; turn: var Turn):
  connectStdio(root, turn)
  let mpd = newMPDClient()

  onPublish(turn, root, ?ListenOn) do (a: Assertion):
    stderr.writeLine "got listen-on ", a
    let ds = unembed a
    onMessage(turn, ds, ?XdgOpen) do (uris: seq[string]):
      for uri in uris: mpd.add uri

runForever()
