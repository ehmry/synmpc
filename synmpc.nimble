# Package

version       = "0.1.0"
author        = "Emery Hemingway"
description   = "Syndicate MPD client"
license       = "Unlicense"
srcDir        = "src"
bin           = @["synmpc"]


# Dependencies

requires "nim >= 1.6.4", "syndicate >= 1.2.1", "mpdclient"
